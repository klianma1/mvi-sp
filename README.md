# Scientific articles multilabel classification

<b>Task:</b> Create a multilabel classificator based on arXiv Dataset (https://www.kaggle.com/Cornell-University/arxiv). The data was downloaded straight from the Kaggle page. After this, the data was uploaded onto Google Drive, and then were accessed from the Google Collab. All the data manipulation was performed in the Google Collab notebook (see file <b>semestral_notebook/Sem_prace_klianma1.ipynb</b> in the repo root). All the steps can be repeated in the notebook. See the detailed report in the <b>report.pdf</b> file.
