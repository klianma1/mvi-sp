# arXiv recommender

__Úkolem moje práce je tvorba doporučovatelu.__

__Data:__ úložiště 1,7 milionu článků. Obsahují názvy článků, jména autoři, kategorie, abstrakty a více. Pro svoje práce jsem vytvořila tabulku, ve kterou mám sloupec “text” a 20 cloupců s cílovými proměnými.  

__Postup:__ Sloupec „text“ byl vytvořen ze třech původních sloupců s jmény autoři, názvy článků a abstraktem. Tyto sloupce byli předzpracovany (vymazany „\n“, „.“ atd.) a pak sloučeny do jednoho textu. Sloupce s cílovými proměnými byly získány ze sloupce „categories“ původních dat, nazvy byly převzaty [tady](https://arxiv.org/category_taxonomy). Sloupce obsahují hodnoty 0 a 1, jeden text muže patřit do několik kategorií.

Považuji to za svůj úkol vytvořit embedding textu pomocí word2vec, pak udělat multilabel classification pomocí rekurentní neuronové sítě (asi s jednou nebo dvěma vyrstvy LSTM). (Protože jenom před týdnem bylo rozhodnuto změnit svůj úkol z učení bez učitele (shlukovaní) na učeni z učitelem (multilabel classification) jsem musela smazat natrenovanou sít pro word2wec, protože jsem dělala jí na všech datech, a ted´ musím jenom na trenovací časti dat. Také vytažení cílových hodnot ze sloupce ‘categories’ trvalo několik dní vůči velkému datasetu).


Pro pochopeni problemu jsem přečetla nasledujici članky:
- https://gist.github.com/aparrish/2f562e3737544cf29aaf1af30362f469#file-understanding-word-vectors-ipynb
- https://keras.io/examples/nlp/pretrained_word_embeddings/
- https://radimrehurek.com/gensim/models/word2vec.html 
- https://www.nltk.org/index.html 
- https://machinelearningmastery.com/multi-label-classification-with-deep-learning/
- https://towardsdatascience.com/building-a-multi-label-text-classifier-using-bert-and-tensorflow-f188e0ecdc5d
- https://stanford.edu/~shervine/teaching/cs-230/cheatsheet-recurrent-neural-networks
- https://towardsdatascience.com/illustrated-guide-to-lstms-and-gru-s-a-step-by-step-explanation-44e9eb85bf21
